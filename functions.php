<?php

function grab_script_enqueue() {
	wp_enqueue_style('materialcss', get_template_directory_uri() . '/css/material.min.css', array(), '1.0.0', 'all');
	wp_enqueue_style('grabstyle', get_template_directory_uri() . '/css/grab.css', array(), '1.0.0', 'all');
	wp_enqueue_style('stylecss', get_template_directory_uri() . '/style.css', array(), '1.0.0', 'all');
	wp_enqueue_style('googlecss-roboto','https://fonts.googleapis.com/css?family=Roboto:regular,bold,italic,thin,light,bolditalic,black,medium&amp;lang=en',array(), '1.0.0', 'all');
	wp_enqueue_style('googlecss-material','https://fonts.googleapis.com/icon?family=Material+Icons',array(), '1.0.0', 'all');
}

// Add Scripts
add_action( 'wp_enqueue_scripts', 'grab_script_enqueue');
