<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="description" content="A portfolio template that uses Material Design Lite.">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0">
	<title>Grab Jet</title>
	<?php wp_head(); ?>
</head>
<body>
	<div class="mdl-layout mdl-js-layout mdl-layout--fixed-header">
      <header class="mdl-layout__header mdl-layout__header--waterfall portfolio-header">
        <div class="mdl-layout__header-row portfolio-logo-row">
          <span class="mdl-layout__title">
            <div class="portfolio-logo"></div>
            <span class="mdl-layout__title">JET</span>
          </span>
        </div>
        <div class="mdl-layout__header-row portfolio-navigation-row mdl-layout--medium-screen-only">
					<nav class="mdl-navigation mdl-typography--body-1-force-preferred-font">
	          <a class="mdl-navigation__link is-active" href="#">GrabJet</a>
						<a class="mdl-navigation__link" href="#">GrabLimo</a>
	          <a class="mdl-navigation__link" href="#">GrabCruise</a>
	          <a class="mdl-navigation__link" href="#">GrabCopter</a>
	        </nav>
        </div>
      </header>
      <div class="mdl-layout__drawer mdl-layout--small-screen-only">
				<nav class="mdl-navigation mdl-typography--body-1-force-preferred-font">
					<a class="mdl-navigation__link is-active" href="#">GrabJet</a>
					<a class="mdl-navigation__link" href="#">GrabLimo</a>
					<a class="mdl-navigation__link" href="#">GrabCruise</a>
					<a class="mdl-navigation__link" href="#">GrabCopter</a>
				</nav>
      </div>
