<?php get_header(); ?>

      <main class="mdl-layout__content">

        <div class="mdl-grid portfolio-max-width portfolio-contact">

          <div class="mdl-cell mdl-cell--12-col mdl-card mdl-shadow--4dp">

            <div class="mdl-card__title">

              <h2 class="mdl-card__title-text">Grab Your Jet</h2>

            </div>

            <div class="mdl-card__media">

              <img class="article-image" src= <?php echo get_template_directory_uri(). "/images/airplane.gif" ?> border="0" alt="">

            </div>

            <div class="mdl-card__actions mdl-card--border">

              <form action="#" class="">

                <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">

                  <input class="mdl-textfield__input" pattern="[A-Z,a-z, ]*" type="text" id="name">

                  <label class="mdl-textfield__label" for="from">Name...</label>

                  <span class="mdl-textfield__error">Letters and spaces only</span>

                </div>

                <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">

                  <input class="mdl-textfield__input" type="email" id="Email">

                  <label class="mdl-textfield__label" for="Email">Email...</label>

                </div>

                <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">

                  <input class="mdl-textfield__input" pattern="[A-Z,a-z, ]*" type="text" id="pickup">

                  <label class="mdl-textfield__label" for="to">PICK-UP</label>

                  <span class="mdl-textfield__error">Letters and spaces only</span>

                </div>

                <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">

                  <input class="mdl-textfield__input" pattern="[A-Z,a-z, ]*" type="text" id="drop">

                  <label class="mdl-textfield__label" for="Name">DROP-OFF</label>

                  <span class="mdl-textfield__error">Letters and spaces only</span>

                </div>

                <div class="mdl-grid">

                  <button id="show-dialog-addons" type="button" class="mdl-cell mdl-cell--6-col mdl-cell--8-col-tablet mdl-button mdl-js-button mdl-button--raised mdl-button--accent">Addons</button>

                  <button id="show-dialog-date" type="button" class="mdl-cell mdl-cell--6-col mdl-cell--8-col-tablet mdl-button mdl-js-button mdl-button--raised mdl-button--accent">Select Date</button>

                  <button id="request-jet-button" class="mdl-cell mdl-cell--12-col mdl-button  mdl-button--raised mdl-button--accent" type="button">

                     Request a Jet

                  </button>

                </div>

              </form>

            </div>

          </div>

        </div>

<?php get_footer(); ?>

