<footer class="mdl-mini-footer">

	<div class="mdl-mini-footer__left-section">

		<ul class="mdl-mini-footer__link-list">

			<li><a href="javascript:void()">GrabLimo</a></li>

			<li><a href="javascript:void()">GrabCruise</a></li>

			<li><a href="javascript:void()">GrabCopter</a></li>

		</ul>

	</div>

	<div class="mdl-mini-footer__right-section">

		<div class="mdl-logo">

			<p>&copy; Copyright Grab.com <?php date("Y"); ?>. All Rights Reserved

			<p>

		</div>

	</div>

</footer>

</main>

</div>

<script type="text/javascript">

function initMap() {

google.maps.event.addDomListener(window, 'load', function () {

var pickup = new google.maps.places.Autocomplete(document.getElementById('pickup'));



var drop = new google.maps.places.Autocomplete(document.getElementById('drop'));

// google.maps.event.addListener(places, 'place_changed', function () {

//     var place = places.getPlace();

//     var address = place.formatted_address;

//     var latitude = place.geometry.location.lat();

//     var longitude = place.geometry.location.lng();

//      var mesg = "Address: " + address;

//     mesg += "\nLatitude: " + latitude;

//     mesg += "\nLongitude: " + longitude;

//     alert(mesg);

});

}

</script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/dialog-polyfill/0.4.2/dialog-polyfill.min.js"></script>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

<script src="https://code.getmdl.io/1.3.0/material.min.js"></script>

<script async defer type="text/javascript" src="https://maps.googleapis.com/maps/api/js?libraries=places&key=AIzaSyCmz__j8JC5nzTKjf2-_WDYy8OAwb0n74s&callback=initMap"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/rome/2.1.22/rome.js"></script>

<script src=<?php echo get_template_directory_uri(). "/js/moment.min.js" ?> ></script>

<script src=<?php echo get_template_directory_uri(). "/js/mdl-datepicker.js"?>></script>

<script src=<?php echo get_template_directory_uri(). "/js/grab.js"?>></script>





<dialog id="dialog-addon" class="mdl-dialog">

<h4 class="mdl-dialog__title">Select Our Premium Services?</h4>

<div class="mdl-dialog__content">

<p>

	Choose Addons to make your trip memorable.

</p>

<label class="mdl-checkbox mdl-js-checkbox mdl-js-ripple-effect" for="attendant">

<input type="checkbox" data-addon-value="300" id="attendant" class="mdl-checkbox__input">

<span class="mdl-checkbox__label">Flight Attendant (+ $300)</span>

</label>

<label class="mdl-checkbox mdl-js-checkbox mdl-js-ripple-effect" for="kitchen">

<input type="checkbox" data-addon-value="100" id="kitchen" class="mdl-checkbox__input">

<span class="mdl-checkbox__label">Full Kitchen (+ $100)</span>

</label>

<label class="mdl-checkbox mdl-js-checkbox mdl-js-ripple-effect" for="massage">

<input type="checkbox" data-addon-value="50" id="massage" class="mdl-checkbox__input">

<span class="mdl-checkbox__label">Massage (+ $50)</span>

</label>

<label class="mdl-checkbox mdl-js-checkbox mdl-js-ripple-effect" for="jacuzzi">

<input type="checkbox" data-addon-value="180" id="jacuzzi" class="mdl-checkbox__input">

<span class="mdl-checkbox__label">Jacuzzi (+ $180)</span>

</label>

</div>

<div class="mdl-dialog__actions">

<button type="button" class="mdl-button close">Confirm</button>

</div>

</dialog>



<dialog id="dialog-date" class="mdl-dialog">

 <h4 class="mdl-dialog__title">Select Our Premium Services?</h4>

 <div class="mdl-dialog__content">

	 <p>

		 Choose a date to make your trip memorable.

	 </p>

	 <div id="date_container" class="mdl-textfield mdl-js-textfield is-upgraded">

			 <input class="mdl-textfield__input" type="text" id="date" readonly />

			 <label class="mdl-textfield__label" id="date_label" for="date">Date</label>

	 </div>

 </div>

 <div class="mdl-dialog__actions">

	 <button type="button" class="mdl-button close">Confirm</button>

 </div>

</dialog>

</body>

</html>

